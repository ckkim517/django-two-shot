from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.contrib.auth.decorators import login_required

# Create your views here.


# a view that will get all of the instances of the Receipt model
# put them in the context for the template
@login_required
def show_receipt(request):
    show_receipt = Receipt.objects.filter(purchaser=request.user)
    # every key in the context dictionary is a variable in the html template
    context = {
        "show_receipt": show_receipt,
    }
    return render(request, "receipts/list.html", context)


# a view that will allow someone to enter Receipt properties
@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


# ExpenseCategory list view
@login_required
def category_list(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": category_list,
    }
    return render(request, "receipts/category.html", context)


# Account list view
@login_required
def account_view(request):
    account_view = Account.objects.filter(owner=request.user)
    context = {
        "account_view": account_view,
    }
    return render(request, "receipts/account.html", context)


# a view that will allow someone to enter ExpenseCategory properties
@login_required
def create_expense_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_expense.html", context)


# a view that will allow someone to enter Account properties
@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("account_view")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
